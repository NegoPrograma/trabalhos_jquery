$(document).ready(function(){
    $('.cpf').mask('000.000.000-00', {reverse: true});
    $('.phone_with_ddd').mask('(00) 0000-0000');
    $('.ip_address').mask('099.099.099.099');
});